
// var DEFAULT_URL = "https://bill-auger.firebaseapp.com/" ;
var DEFAULT_URL = "http://duckduckgo.com" ;
var NEW_WINDOW_OPTIONS = "chrome,extrachrome,menubar,resizable,scrollbars,status,toolbar" ;
var Browser ; // onload()


function openNewWindow(url) { window.open(url , "_blank" , NEW_WINDOW_OPTIONS) ; }

function onload()
{
  Browser = document.getElementById('main-browser') ;
  Browser.loadURI(DEFAULT_URL , null , null) ;

[
//   "chrome://global/content/console.xul" ,                                // OK
//   "chrome://global/content/config.xul" ,                                 // OK
//   "chrome://mozapps/content/extensions/extensions.xul?type=extensions" , // NFG
//   "chrome://mozapps/content/extensions/extensions.xul" ,                 // NFG
//   "view-source:chrome://branding/locale/brand.dtd"                       // OK
].forEach(openNewWindow) ;
}


addEventListener('load' , onload , false) ;
